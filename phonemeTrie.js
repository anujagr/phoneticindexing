var phonemeTrie = new (require('./trie').Trie)();
var phonemeData = new (require('./phonemeFinder').PhonemeData)();
var phonemeFinder = new (require('./phonemeFinder').PhonemeFinder)();
var phonemeDataPopulator = require('./phonemeDataPopulator');

function createPhonemeTrie(){
    var wordList = ["045", "145", "245", "345"];

    wordList.forEach(function(word){
        phonemeTrie.addValue(word);
    });

    phonemeTrie.printWordsInTrie();
}

function populatePhonemeData(){
    var phonemeNumberToFileMap = {'0': 'data\\b3.wav', '1': 'data\\hh.wav', '2': 'data\\k1.wav', '3': 'data\\m.wav', '4': 'data\\ae1.wav', '5': 'data\\t1.wav'};

    phonemeDataPopulator.setPhonemeData(phonemeNumberToFileMap, phonemeData, function(err){
        if (err){
            console.error("Error populating phoneme data: %s", err);
            return;
        }

        phonemeData.printPhonemeData();

        identifyWord("cat.wav");
    });
}

function identifyWord(filename){
    var currentTrie = phonemeTrie;
    var phonemeFound = '';
    var wordFound = '';

    phonemeFinder.init(filename, phonemeData, function(err){
        if (err){
            console.error("Error encountered while initialising phonemeFinder: %s", err);
            return;
        }

        do{
            var nextPhonemes = [];

            for (var key in currentTrie.children){
                nextPhonemes.push(key);
            }

            phonemeFound = phonemeFinder.findPhoneme(nextPhonemes);
            wordFound += phonemeFound;

            if (phonemeFound != ''){
                currentTrie = currentTrie.children[phonemeFound];
            }
        } while (phonemeFound != '');

        console.log("Found word: ", wordFound);
    });
}

createPhonemeTrie();
populatePhonemeData();
