PhonemeData = function(){
    this.phonemeToMFCC = [];
    this.numberToPhonemeMap = { '0' : 'B', '1': 'HH', '2': 'K', '3': 'M', '4' : 'AE', '5' : 'T'};
    this.phonemeToDurationMs = [];
    this.mfccCalculator = new (require('./mfccCalculator').MFCCCalculator)();
};

PhonemeData.prototype.getFrameRateAndSignal = function(filename, callback)
{
    var fs = require('fs');
    var self = this;
    fs.readFile(filename, function(err, audioBuffer) {
        if (err) {
            console.error("Error: %s encountered while reading audio file: %s", err, filename);
            return callback.apply(self,[err]);
        }
        var wav = require('node-wav');
        var result = wav.decode(audioBuffer);

        var signal = result.channelData[0];
        var frameRate = result.sampleRate;

        return callback.apply(self, [null, frameRate, signal]);
    });

    return;
}

PhonemeData.prototype.printPhonemeData = function(){
    console.log(this.phonemeToMFCC);
    console.log(this.phonemeToDurationMs);
    console.log(this.numberToPhonemeMap);
};

PhonemeData.prototype.setPhonemeDurationAndMFCC = function(phonemeNumber, filename, callback){
    this.getFrameRateAndSignal(filename, function(err, frameRate, signal){
        if (err){
            console.error("Error: %s while reading audio file: %s", err, filename);
            return callback(err);
        }

        var mfccCoefficients = this.mfccCalculator.calculateMFCC(signal, frameRate);
        var phoneme = this.numberToPhonemeMap[phonemeNumber];
        this.phonemeToMFCC[phoneme] = mfccCoefficients;
        this.phonemeToDurationMs[phoneme] = parseInt((parseFloat(signal.length) / frameRate) * 1000);
        //this.phonemeToDurationMs[phoneme] = 220;
        return callback(null);
    });
};

PhonemeData.prototype.getMFCCForPhoneme = function(phonemeNumber){
    var phoneme = this.numberToPhonemeMap[phonemeNumber];
    return this.phonemeToMFCC[phoneme];
};

PhonemeData.prototype.getPhonemeDurationInMs = function(phonemeNumber){
    var phoneme = this.numberToPhonemeMap[phonemeNumber];
    return this.phonemeToDurationMs[phoneme];
};

PhonemeFinder = function() {
    this.signal = [];
    this.sampleRate = 0;
    this.currentPosition = 0;
    this.phonemeData = null;
    this.mfccCalculator = new (require('./mfccCalculator').MFCCCalculator)();
};

PhonemeFinder.prototype.getFrameRateAndSignal = function(filename, callback)
{
    var fs = require('fs');
    var self = this;
    fs.readFile(filename, function(err, audioBuffer) {
        if (err) {
            console.error("Error: %s encountered while reading audio file: %s", err, filename);
            return callback.apply(self,[err]);
        }
        var wav = require('node-wav');
        var result = wav.decode(audioBuffer);

        var signal = result.channelData[0];
        var frameRate = result.sampleRate;

        return callback.apply(self, [null, frameRate, signal]);
    });

    return;
}

PhonemeFinder.prototype.init = function(filename, phonemeData, callback){
    this.phonemeData = phonemeData;

    this.getFrameRateAndSignal(filename, function(err, frameRate, signal){
        if (err){
            console.error("Error: %s encountered while reading audio file: %s", err, filename);
            return callback(err);
        }

        this.signal = signal;
        this.sampleRate = frameRate;

        return callback(null);
    });
};

PhonemeFinder.prototype.findPhoneme = function(phonemeList){
    var minScore = Infinity;
    var matchedPhoneme = '';

    for (var i = 0; i < phonemeList.length; i++){
        var phonemeNumber = phonemeList[i];
        var score = this.getPhonemeMatchingScore(phonemeNumber);

        if (score != -1 && score < minScore){
            minScore = score;
            matchedPhoneme = phonemeNumber;
        }
    }

    if (matchedPhoneme != '') {
        var durationToMoveMs = this.getDurationForMatchedPhoneme(matchedPhoneme);
        this.currentPosition += parseInt((parseFloat(durationToMoveMs) / 1000) * this.sampleRate);
    }

    return matchedPhoneme;
};

function getMFCCComparisonScore(expectedMFCC, actualMFCC){
    if (expectedMFCC.length != actualMFCC.length)
        return -1;

    var totalScore = 0.0;

    for (var i = 0; i < expectedMFCC.length; i++){
        var diff = parseFloat(expectedMFCC[i] - actualMFCC[i]);
        totalScore += Math.pow((diff/ expectedMFCC[i]), 2);
    }

    return Math.sqrt(totalScore);
}

PhonemeFinder.prototype.getPhonemeMatchingScore = function(phonemeNumber){
    var expectedMFCCForPhoneme = this.phonemeData.getMFCCForPhoneme(phonemeNumber);
    var durationForPhonemeInMs = this.phonemeData.getPhonemeDurationInMs(phonemeNumber);

    var phonemeSignalStart = this.currentPosition;
    var phonemeSignalEnd = this.currentPosition + parseInt((parseFloat(durationForPhonemeInMs) / 1000) * this.sampleRate);

    if (phonemeSignalEnd > this.signal.length)
        return -1;

    var signalForPhoneme = this.signal.slice(phonemeSignalStart, phonemeSignalEnd);
    var actualMFCCForPhoneme = this.mfccCalculator.calculateMFCC(signalForPhoneme, this.sampleRate);

    console.log("For phoneme :%s Expected MFCC: %s", phonemeNumber, expectedMFCCForPhoneme);
    console.log("For phoneme :%s acutal MFCC: %s", phonemeNumber, actualMFCCForPhoneme);
    var score = getMFCCComparisonScore(expectedMFCCForPhoneme, actualMFCCForPhoneme);
    console.log("For phoneme: %s score: %s", phonemeNumber, score);

    return score;
};

PhonemeFinder.prototype.getDurationForMatchedPhoneme = function(phonemeNumber){
    // TODO: Find the distance to move where MFCC is max matched.
    return this.phonemeData.getPhonemeDurationInMs(phonemeNumber);
};

exports.PhonemeFinder = PhonemeFinder;
exports.PhonemeData = PhonemeData;