MFCCCalculator = function(){
    this.lowFrequency = 300;
    this.highFrequency = 8000;
    this.filterBankCount = 26;
    this.fft = require('fft-js');
    this.dct = require('dct');
    this.mfccCoefficientsCount = 13;
};

function convertSignalToPowerOfTwo(signal){
    var desiredLength = Math.pow(2, Math.ceil(Math.log(signal.length) / Math.log(2)));
    var newSignal = new Float32Array(desiredLength);

    for (var i = 0; i < signal.length; i++){
        newSignal[i] = signal[i];
    }

    for (var i = signal.length; i < desiredLength; i++){
        newSignal[i] = 0.0000;
    }

    return newSignal;
}

MFCCCalculator.prototype.calculateMFCC = function(signal, sampleRate){
    var newSignal = convertSignalToPowerOfTwo(signal);
    var phasors = this.fft.fft(newSignal);
    var phasorMagnitudes = this.fft.util.fftMag(phasors);
    var filterBanks = this.constructMelFilterBank(phasorMagnitudes.length, sampleRate);
    return this.getMFCCCoefficients(phasorMagnitudes, filterBanks);
};

MFCCCalculator.prototype.constructMelFilterBank = function(fftSize, sampleRate) {
    var bins = [],
        fq = [],
        filters = [];

    var lowM = hzToMels(this.lowFrequency),
        highM = hzToMels(this.highFrequency),
        deltaM = (highM - lowM) / (this.filterBankCount + 1);

    // Construct equidistant Mel values between lowM and highM.
    for (var i = 0; i < this.filterBankCount; i++) {
        // Get the Mel value and convert back to frequency.
        // e.g. 200 hz <=> 401.25 Mel
        fq[i] = melsToHz(lowM + (i * deltaM));

        // Round the frequency we derived from the Mel-scale to the nearest actual FFT bin that we have.
        // For example, in a 64 sample FFT for 8khz audio we have 32 bins from 0-8khz evenly spaced.
        bins[i] = Math.floor(((fftSize + 1) * fq[i]) / sampleRate);
    }

    // Construct one cone filter per bin.
    // Filters end up looking similar to [... 0, 0, 0.33, 0.66, 1.0, 0.66, 0.33, 0, 0...]
    for (var i = 0; i < bins.length; i++) {
        filters[i] = [];
        var filterRange = (i != bins.length - 1) ? bins[i + 1] - bins[i] : bins[i] - bins[i - 1];
        filters[i].filterRange = filterRange;
        for (var f = 0; f < fftSize; f++) {
            // Right, outside of cone
            if (f > bins[i] + filterRange) filters[i][f] = 0.0;
            // Right edge of cone
            else if (f > bins[i]) filters[i][f] = 1.0 - ((f - bins[i]) / filterRange);
            // Peak of cone
            else if (f == bins[i]) filters[i][f] = 1.0;
            // Left edge of cone
            else if (f >= bins[i] - filterRange) filters[i][f] = 1.0 - (bins[i] - f) / filterRange;
            // Left, outside of cone
            else filters[i][f] = 0.0;
        }
    }

    // Store for debugging.
    filters.bins = bins;

    return filters;

};

MFCCCalculator.prototype.getMFCCCoefficients = function(fft, filterBanks){
    var melSpec = [];

    filterBanks.forEach(function (filterBank, filterIndex) {
        var tot = 0;
        fft.forEach(function (fp, frequencyIndex) {
            tot += fp * filterBank[frequencyIndex];
        });
        melSpec[filterIndex] = tot;
    });

    var melSpecLog = melSpec.map(function(value){
        return Math.log(1 + value);
    });

    var mfccCoefficients = this.dct(melSpecLog).slice(0, this.mfccCoefficientsCount);

    return mfccCoefficients;
}

function melsToHz(mels) {
    return 700 * (Math.exp(parseFloat(mels) / 1125) - 1);
}

function hzToMels(hertz) {
    return 1125 * Math.log(1 + parseFloat(hertz) / 700);
}

exports.MFCCCalculator = MFCCCalculator;