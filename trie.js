Trie = function() {
    this.value = "";
    this.children = [];
};


/**
 * Add a value to the trie
 *
 * @param {String} value
 * @param {Number} index (optional)
 */
Trie.prototype.addValue = function(item, index) {
    if (!index) {
        index = 0;
    }
    if(item === null) {
        return;
    }

    if(item.length === 0) {
        return;
    }

    if (index === item.length) {
        this.value = item;
        return;
    }

    var key = item[index];
    if (this.children[key] === undefined) {
        this.children[key] = new Trie();
    }
    var child = this.children[key];
    child.addValue(item, index + 1);
};

/**
 * Remove a value form the trie
 *
 * @param {String} value
 * @param {Number} index (optional)
 */
Trie.prototype.removeValue = function(item, index) {
    if (!index) {
        index = 0;
    }

    if (item.length === 0) {
        return;
    }

    if (index === item.length) {
        this.value="";
    }
    else {
        var key = item[index];
        var child = this.children[key];
        if(child) child.removeValue(item, index + 1);
        // to remove a node, we need remove it from parent's children array
        if(index === (item.length - 1)) {
            if(Object.keys(child.children).length === 0) {// only remove when there is no children
                delete this.children[key];
            }
        }
    }
};

Trie.prototype.printWordsInTrie = function(){
    if (this.value != ""){
        console.log(this.value);
        return;
    }

    if (this.children.length == 0){
        return;
    }

    for (var key in this.children){
        var child = this.children[key];
        child.printWordsInTrie();
    }
}

exports.Trie = Trie;