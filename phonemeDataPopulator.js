module.exports = {
    setPhonemeData : function(phonemeNumberToFileList, phonemeData, callback) {
        var requestsInFlight = 0;
        var error = '';

        for (var phonemeNumber in phonemeNumberToFileList) {
            requestsInFlight++;
            phonemeData.setPhonemeDurationAndMFCC(phonemeNumber, phonemeNumberToFileList[phonemeNumber], function (err) {
                requestsInFlight--;
                if (err) {
                    console.error("Not able to populate phoneme data for phonemeNumber: %s due to error: %s", phonemeNumber, err);
                    error += err;
                }

                if (requestsInFlight == 0){
                    if (error != '')
                        return callback(error);
                    else
                        return callback(null);
                }
            });
        }
    }
};